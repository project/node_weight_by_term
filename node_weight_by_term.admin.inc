<?php

/**
 * @file
 * Node weight by term administration pages functionality.
 */

/**
 * Overview page for "Node weight by term" taxonomy-term-reference fields.
 */
function node_weight_by_term_overview_page() {
  $settings = variable_get('node_weight_by_term', array());

  // If only one field uses Node weight by term, skip overview page.
  if (count($settings) == 1) {
    drupal_goto('admin/structure/node-weight-by-term/' . key($settings));
  }

  if (count($settings) == 0) {
    return t('No taxonomy term fields found which have node weight by term enabled.');
  }

  // Loop over all fields using Node weight by term and display link.
  $header = array(t('Field name'), array('data' => t('Operations')));
  $rows = array();
  foreach ($settings as $managed_field => $vid) {
    $row = array();
    $row[] = _node_weight_by_term_get_field_label($managed_field);
    $row[] = l(t('manage weights'), 'admin/structure/node-weight-by-term/' . $managed_field);
    $rows[] = $row;
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}


/**
 * Form for the administration form to manage weights.
 *
 * @param string $field_name
 *   field name that uses node term by weight
 *
 * @see node_weight_by_term_category_form_submit
 */
function node_weight_by_term_category_form($form, $form_state, $field_name) {
  $form = array();
  $settings = variable_get('node_weight_by_term', array());

  // Go back to overview if there are no settings for this field.
  if (!isset($settings[$field_name])) {
    drupal_goto('admin/structure/node-weight-by-term');
  }

  // Set page title
  drupal_set_title(_node_weight_by_term_get_field_label($field_name));


  $vocabulary_id = $settings[$field_name];


  // Get all terms in a select list.
  $terms = taxonomy_get_tree($vocabulary_id);
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manage weight for'),
    '#theme' => 'exposed_filters',
  );

  $form['filters']['tid'] = array(
    '#title' => t('Term'),
    '#type' => 'select',
    '#options' => _node_weight_by_term_render_as_options($terms),
    '#required' => TRUE,
    '#attributes' => array(
      'onchange' => 'javascript:window.location=\'' . url('admin/structure/node-weight-by-term/' . arg(3)) . '?tid=\'+this.value;',
    ),
    '#value' => !empty($_GET['tid']) ? $_GET['tid'] : '',
  );

  $form['node_items'] = array(
    '#tree' => TRUE,
    '#type' => 'table',
    '#weight' => -100,
  );

  // Fallback when no term argument given.
  if (empty($_GET['tid'])) {
    return $form;
  }

  // Get existing weights.
  $result = db_query("
    SELECT
    c.entity_id AS 'nid',
    n.title AS 'title',
    c." . $field_name . "_tid AS 'tid',
    IFNULL(w.weight, 0) AS 'weight'
    FROM field_data_" . $field_name . " AS c
    INNER JOIN node n ON c.entity_id = n.nid
    LEFT OUTER JOIN node_weight_by_term w ON (w.nid = n.nid AND w.tid = c." . $field_name . "_tid)
    WHERE n.status = 1
      AND c.revision_id = n.vid
      AND c." . $field_name . "_tid = " . $_GET['tid'] . "
    ORDER BY weight ASC");

  // Iterate through each database result.
  foreach ($result as $item) {

    // Create a form entry for this item.
    // Each entry will be an array using the the unique id for that item as
    // the array key, and an array of table row data as the value.
    $form['node_items'][$item->nid] = array(

      // Column name.
      'name' => array(
        '#markup' => check_plain($item->title),
      ),
      // The 'weight' field will be manipulated as we move the items around in
      // the table using the tabledrag activity.  We use the 'weight' element
      // defined in Drupal's Form API.
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $item->weight,
        '#delta' => 500,
        '#title-display' => 'invisible',
      ),
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );
  return $form;
}


/**
 * Renders an array of terms as an option array which can be used in a form.
 *
 * @param array $terms
 *   An array of terms
 * @param int $parent
 *   Internally used in recursion, no need to specify this manually
 * @param int $level
 *   Internally used in recursion, no need to specify this manually
 *
 * @return array
 *   An array with term ids as keys and term names as values
 */
function _node_weight_by_term_render_as_options($terms, $parent = 0, $level = 0) {
  $list = array();
  foreach ($terms as $term) {
    if ($term->parents[0] == $parent) {
      $prefix = '';
      for ($i = 0; $i < $level; $i++) {
        $prefix .= ' - ';
      }
      $list[$term->tid] = $prefix . $term->name;
      $subs = _node_weight_by_term_render_as_options($terms, $term->tid, $level + 1);
      foreach ($subs as $tid => $sub) {
        $list[$tid] = $sub;
      }
    }
  }
  return $list;
}
