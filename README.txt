NODE WEIGHT BY TERM
======================

This module allows you to set a custom weight on a node for each term it is
related to.


USECASE EXAMPLE
======================

When you have a view which shows product nodes, and you want the product nodes
ordered depending on each product category, which is a taxonomy term.


USAGE
======================

1. Enable module

2. Go to the taxonomy term reference field settings and enable the checkbox
   "Enable custom weight for nodes tagged with terms using this field".
   (e.g. /admin/structure/types/manage/yournodetype/fields/field_your_fieldname)

3. In your node-listing view, add a relation to
   "Node weight by term: Custom weight" and set this relation as required.

4. Change default sort in views to Node weight by term: Weight (Use the newly
   created relation in step 3 and leave order to ascending).


AUTHOR/MAINTAINER
======================

- Jimmy Henderickx  (StryKaizer)
- Joris Vercammen Henderickx  (borrison_)
