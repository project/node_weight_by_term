<?php

/**
 * @file
 * Views integration code for node weight by term.
 */


/**
 * Implements hook_views_data().
 */
function node_weight_by_term_views_data() {
  $data['node_weight_by_term']['table']['group'] = 'Node weight by term';

  $data['node_weight_by_term']['table']['base'] = array(
    'field' => 'wid',
    'title' => 'Node weight by term',
    'help' => 'Custom weights for each taxonomy term',
    'weight' => -10,
  );

  $data['node_weight_by_term']['table']['join'] = array(
    'node' => array(
      'left table' => 'node',
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['node_weight_by_term']['wid'] = array(
    'title' => 'Node weight by term: id',
    'help' => 'Node weight by term: id',
  );

  $data['node_weight_by_term']['tid'] = array(
    'title' => 'Node weight by term: Term id',
    'help' => 'Node weight by term: Term id',
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'taxonomy_term_data',
      'base field' => 'tid',
      'handler' => 'views_handler_relationship',
      'title' => 'Taxonomy term for custom weight',
      'help' => 'Relates nodes to the custom weight set by Node weight by term',
    ),
  );

  $data['node_weight_by_term']['nid'] = array(
    'title' => 'Node weight by term: Node id',
    'help' => 'Node weight by term: Node nid',
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'title' => 'Node for custom weight',
      'help' => 'Relate nodes to a custom weight set by Node weight by term',
    ),
  );

  $data['node_weight_by_term']['weight'] = array(
    'title' => 'Node weight by term: Weight',
    'help' => 'Weight for term/node combination',
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
